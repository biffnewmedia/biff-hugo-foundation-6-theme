$(document).foundation();

$.fn.countdown = function(options) {
 var settings = { 'date': null };
 if(options) {
  $.extend(settings, options);
 }

 var this_sel = $(this);

 function count_exec() {
  var eventDate = Date.parse(settings['date']) / 1000; // Parse the date string
  var currentDate = 	Math.floor($.now() / 1000); // Find the timestamp for now
  var seconds = eventDate - currentDate; // Find the number of seconds remaining
  var days;
  var hours;
  var minutes;

  //console.log('seconds', seconds)

  if (seconds <= 0) { // After the event date has passed
   days = 0;
   hours = 0;
   minutes = 0;
   seconds = 0;
  } else {
   days = 			Math.floor(seconds / (60 * 60 * 24));		// Divide to find the number of days remaining
   seconds -=		days * 60 * 60 * 24;						// Subtract the number of (complete, => 24 hours) days calculated above

   hours = 		Math.floor(seconds / (60 * 60));			// Get the number of hours from that modified number ^
   seconds -=		hours * 60 * 60;

   minutes = 		Math.floor(seconds / 60);
   seconds -=		minutes * 60;
  }
  this_sel.find('#days').val(days).trigger('change');
  this_sel.find('#hours').val(hours).trigger('change');
  this_sel.find('#mins').val(minutes).trigger('change');
  this_sel.find('#secs').val(seconds).trigger('change');

 } // End of count_exec();

 count_exec();

 var interval = setInterval(count_exec, 1000);

} // End of the main function


var magnifPopup = function() {
    $('.image-popup').magnificPopup({
        type: 'image',
        removalDelay: 300,
        mainClass: 'mfp-with-zoom',
        gallery:{
            enabled:true
        },
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',
            opener: function(openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        }
    });
};

var magnifVideo = function() {
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });
};

var initParticles = function() {

 particlesJS("particles-js", {
  "particles": {
   "number": {
    "value": 100,
    "density": {
     "enable": true,
     "value_area": 800
    }
   },
   "color": {
    "value": "#ffffff"
   },
   "shape": {
    "type": "circle",
    "stroke": {
     "width": 0,
     "color": "#000000"
    },
    "polygon": {
     "nb_sides": 5
    },
    "image": {
     "src": "img/github.svg",
     "width": 100,
     "height": 100
    }
   },
   "opacity": {
    "value": 0.5,
    "random": false,
    "anim": {
     "enable": false,
     "speed": 1,
     "opacity_min": 0.1,
     "sync": false
    }
   },
   "size": {
    "value": 3,
    "random": true,
    "anim": {
     "enable": false,
     "speed": 40,
     "size_min": 0.1,
     "sync": false
    }
   },
   "line_linked": {
    "enable": true,
    "distance": 150,
    "color": "#ffffff",
    "opacity": 0.4,
    "width": 1
   },
   "move": {
    "enable": true,
    "speed": 6,
    "direction": "none",
    "random": false,
    "straight": false,
    "out_mode": "out",
    "bounce": false,
    "attract": {
     "enable": false,
     "rotateX": 600,
     "rotateY": 1200
    }
   }
  },
  "interactivity": {
   "detect_on": "canvas",
   "events": {
    "onhover": {
     "enable": true,
     "mode": "grab"
    },
    "onclick": {
     "enable": true,
     "mode": "push"
    },
    "resize": true
   },
   "modes": {
    "grab": {
     "distance": 140,
     "line_linked": {
      "opacity": 1
     }
    },
    "bubble": {
     "distance": 400,
     "size": 40,
     "duration": 2,
     "opacity": 8,
     "speed": 3
    },
    "repulse": {
     "distance": 200,
     "duration": 0.4
    },
    "push": {
     "particles_nb": 4
    },
    "remove": {
     "particles_nb": 2
    }
   }
  },
  "retina_detect": true
 });
}



var timecount = function (){



 $(".knob").knob({
  change : function (value) {
   //console.log("change : " + value);
  },
  release : function (value) {
   //console.log(this.$.attr('value'));
   //console.log("release : " + value);
  },
  cancel : function () {
   console.log("cancel : ", this);
  },
  draw : function () {

   // "tron" case
   if(this.$.data('skin') == 'tron') {

    var a = this.angle(this.cv)  // Angle
     , sa = this.startAngle          // Previous start angle
     , sat = this.startAngle         // Start angle
     , ea                            // Previous end angle
     , eat = sat + a                 // End angle
     , r = 1;

    this.g.lineWidth = this.lineWidth;

    this.o.cursor
    && (sat = eat - 0.3)
    && (eat = eat + 0.3);

    if (this.o.displayPrevious) {
     ea = this.startAngle + this.angle(this.v);
     this.o.cursor
     && (sa = ea - 0.3)
     && (ea = ea + 0.3);
     this.g.beginPath();
     this.g.strokeStyle = this.pColor;
     this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
     this.g.stroke();
    }

    this.g.beginPath();
    this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
    this.g.stroke();

    this.g.lineWidth = 2;
    this.g.beginPath();
    this.g.strokeStyle = this.o.fgColor;
    this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
    this.g.stroke();

    return false;
   }
  }
 });

 // Example of infinite knob, iPod click wheel
 var v, up=0,down=0,i=0
  ,$idir = $("div.idir")
  ,$ival = $("div.ival")
  ,incr = function() { i++; $idir.show().html("+").fadeOut(); $ival.html(i); }
  ,decr = function() { i--; $idir.show().html("-").fadeOut(); $ival.html(i); };
 $("input.infinite").knob(
  {
   min : 0
   , max : 20
   , stopper : false
   , change : function () {
   if(v > this.cv){
    if(up){
     decr();
     up=0;
    }else{up=1;down=0;}
   } else {
    if(v < this.cv){
     if(down){
      incr();
      down=0;
     }else{down=1;up=0;}
    }
   }
   v = this.cv;
  }
  });




}

var pageAnimmation = function(){

    if ($('body').data('animation') === true) {
        var elements = $('body').find('section[data-animate="true"]');
        var controller = new ScrollMagic.Controller();
        var sceneArray = [];

        for(var i=0; i < elements.length; i++){
            var id = $(elements[i]).attr("id");
            var temp = new ScrollMagic.Scene({ duration: 0, offset: 0, triggerElement: '#'+id+'', triggerHook: 0.65 })
            sceneArray.push(temp);
            sceneArray[i].setTween(TweenMax.fromTo('#'+id+' .grid-container', 1.5, {autoAlpha:0, yPercent: 100},{autoAlpha:1, yPercent:0, ease: Expo.easeOut}))
            sceneArray[i].addTo(controller);
                //.addIndicators({name: id});
        }

    }
};


var smoothScrolling = function(){
  var element = $('body').find('[data-smooth-scroll]');

  $(element).find('a').on('click', function(event){

   event.preventDefault();
  if (
   location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
   &&
   location.hostname == this.hostname
  ) {

   var target = $(this.hash);
   console.log('smoothsroll', this.hash.slice(1))
   target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

   if (target.length) {
    event.preventDefault();
    $('html, body').animate({
     scrollTop: target.offset().top
    }, 1000, function() {
     // Callback after animation
     // Must change focus!
     var $target = $(target);
     $target.focus();
     if ($target.is(":focus")) { // Checking if the target was focused
      return false;
     } else {
      $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
      $target.focus(); // Set focus again
     };
    });
   }
  }


 })

}

var goToTop = function() {

 $('#backtotop').on('click', function(event){
  console.log('test')
  event.preventDefault();

  $('html, body').animate({
   scrollTop: $('html').offset().top
  }, 500);

  return false;
 });

};

// Submit to newsletter

function submitNewsletterEntry(){

 $('#news_error').hide();
 $('#news_success').hide();

 $('#newsletterform').submit(function (e){

  var formData = $('#newsletterform').serializeArray();
  var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

  $('#news_error').hide();
  $('#news_success').hide()

  e.preventDefault();
  if(testEmail.test( formData[0].value)) {
   $.ajax({
    type: 'POST',
    url: $('#newsletterform').attr('action'),
    data: JSON.stringify({email: formData[0].value})
   })
    .done(function (response) {
     $('#news_success').show()
    })
    .fail(function (error) {
     $('#news_error').show();
    })
  }
  else{
   $('#news_error').show();
  }
 })
}

function sendMail(){
 $('#contact_error').hide();
 $('#contact_success').hide();
 $('#contact-form').submit(function (e){
   e.preventDefault();
  var formData = $('#contact-form').serializeArray();
  console.log(formData);

  var data =  {
    name:formData[0].value,
    email: formData[1].value,
    phone: formData[2].value,
    message: formData[3].value
  }

  $.ajax({
   type: 'POST',
   url: $('#contact-form').attr('action'),
   data: JSON.stringify(data)
  })
   .done(function (response) {
    $('#news_success').show()
   })
   .fail(function (error) {
    $('#news_error').show();
   })
 });
}

// PAGE LOADING
$( window ).on('load', function() {
 console.log('HIDE')
 $('.loading').fadeOut('fast');

});

$(document).ready(function() {
    //initParticles();
    magnifPopup();
    magnifVideo();
    goToTop();
    pageAnimmation();
    smoothScrolling();
    submitNewsletterEntry();
    sendMail();

    if($('body').find('section[data-section="countdown"]')){
     var timer =  $('body').find('section[data-section="countdown"]');
     console.log(timer)
     timecount();
     $('#countdown').countdown( {date: $(timer).data('timer')} );
    }

    if ($('body').hasClass('home')) {
        $('footer').addClass('angle-cut-gray');
    }
});





